# CAVEZ of PHEAR

**ASCII Boulder Dash clone** *(Unofficial repo)*

http://www.x86.no/cavezofphear/

![](http://zenway.ru/uploads/2019/03/phear-002.png) 

**Boulder Dash** game clone for your favorite terminal, the game is made entirely in **ASCII** *(Ncurses TUI)*, screen title taunts you to play.

Games mission is to escape through all the caves and make it out alive. To escape through a cave you will have to find all the diamonds located in it. Once you've found all the diamonds, their powers combined will help you get to the next cave, one step closer to freedom. 

To run **CAVEZ of PHEAR** expand your terminal window to at least **80×25**, and then enter phear. 

You, the player, are represented by the uppercase letter **Z** starting in the upper left corner.

![](http://zenway.ru/uploads/2019/03/phear-003.png) 

Movement - Arrow keys, numpad, or WSAD

Toggle mute - M
Show position - F
Restart level - R
Save - O
Load - L
Pause - P

Diamond - * - 10 points
Money - $ - 100 points
Extra life every 1000 points.

Bombs - %
		B to place 
		T to detonate

Monsters - M
		Rocks and bombs are effective against them,
		but they're lethal if they catch up.

Rocks - O
		Squishing hazard.
		
![](http://zenway.ru/uploads/2019/03/phear-005.png)		

CAVEZ of PHEAR is a fun distraction that is easy to get into, and the ASCII art makes it all the more attractive, Level editor CAVEZ of PHEAR is also fincluded.

![](http://zenway.ru/uploads/2019/03/phear-024.png)

